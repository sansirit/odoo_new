# -*- coding: utf-8 -*-
{
    "name": "Look All models in base",
    "summary": "",
    "version": "11.0.1.0.0",
    "category": "views",
    "website": "",
    "description": """
		1) Look All models in base in  mode list,form pivot
		2) Widget format:
		Examples:
     <field name="qty_available" widget="format" options="{'template': '{0}%','decimal':2}"/>
     <field name="qty_available" widget="format" options="{'color':'red'}"/>
     <field name="qty_available" widget="format" options="{'css':'color=red'}"/>
     <field name="qty_available" widget="format" options="{'css':'background-color=red;color=blue'}"/>
		3) Extend systrayMenu
    """,
    'images': [
        'images/screen.png'
    ],
    "author": "Boris.Gra",
    "license": "LGPL-3",
    "installable": True,
    "depends": [
    ],
    "data": [
        'views/view_models.xml',
        'views/ui_views.xml',
        'data/parameter.xml',
        'security/model_security.xml',
        'security/ir.model.access.csv',
    ],
    'qweb': [
        "static/src/xml/systray.xml",
    ],
}
