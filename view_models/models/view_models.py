# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ViewModels(models.TransientModel):
    _name = 'view.models.wizard'

    model_id = fields.Many2one('ir.model')
    model_table = fields.Char(compute='_compute_model_table')
    sql = fields.Text(default="update model_table set field1='1234', field2=123 where field3='18'")

    @api.onchange('model_id')
    def _compute_model_table(self):
        if self.model_id:
            self.model_table = self.model_id.model.replace('.','_')

    @api.multi
    def view_models_open_window(self):
        data = self.browse(self._ids)[0]
        return {
            'name': "View Model %s (%s)" % (data.model_id.name, data.model_id.model),
            'type': 'ir.actions.act_window',
            'views': [(False, 'tree'), (False, 'form'), (False, 'pivot')],
            'res_model': data.model_id.model,
        }

    @api.multi
    def run_sql(self):
        data = self.browse(self._ids)[0]
        self._cr.execute(data.sql)


class IrModels(models.Model):
    _inherit = 'ir.model'

    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, "%s(%s)" % (record.name, record.model)))
        return result