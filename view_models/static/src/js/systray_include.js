odoo.define('customisations.systray', function (require) {
"use strict";
var SystrayMenu = require('web.SystrayMenu');
var Widget = require('web.Widget');
var Rpc = require('web.rpc');
var Core = require('web.core');
var session = require('web.session');

var SystrayDebug = Widget.extend({
    title:'Same debug',
    img:'fa-bomb',
    template:'systray.template',
    events: {
        "click": "on_click",
    },
    on_click: function (event) {
        var url = window.location.href
        if (url.indexOf('debug')<0)
            var url = window.location.origin+window.location.pathname+'?debug=assets'+window.location.hash
        var win = window.open(url, '_blank'); // '_self'
    },
});
session.user_has_group('base.group_system').then(function(has_group) {
    if(has_group)
        SystrayMenu.Items.push(SystrayDebug);
});

Rpc.query({
    model: "ir.config_parameter",
    method: 'search_read',
    context: {},
    domain: [['key', 'ilike', 'systray.']],
})
.then(function (data) {
   for (var i in  data) {
        var sTray = Widget.extend({
            url:data[i].value,
            title:data[i].key.split('.')[2],
            img:data[i].key.split('.')[1],
            template:'systray.template',
            events: {
                    "click": "on_click",
                },
            on_click: function (event) {
                    window.open(this.url, '_blank');
                },
       });
       SystrayMenu.Items.push(sTray);
  }
});

});
