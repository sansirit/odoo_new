odoo.define('customisations.widgets_extend', function (require) {
"use strict";

var registry = require('web.field_registry');
var basic_fields = require('web.basic_fields');
var field_utils = require('web.field_utils');

String.prototype.format = String.prototype.f = function () {
    var args = arguments;
    return this.replace(/\{\{|\}\}|\{(\d+)\}/g, function (m, n) {
        if (m == "{{") { return "{"; }
        if (m == "}}") { return "}"; }
        return args[n];
    });
};
var ColumnFormat = basic_fields.FieldMonetary.extend({
    _formatValue: function (value) {
        var options = _.extend({}, this.nodeOptions, { data: this.recordData }, this.formatOptions);
        if ('decimal' in options)
            value = value.toFixed(options['decimal'])
        if ('color' in options)
            this.$el.css('color',options['color'])
        if ('css' in options) {
            var ccs_all = options['css'].split(';')
            for (var index in ccs_all)
                this.$el.css(ccs_all[index].split('=')[0],ccs_all[index].split('=')[1])
          }
        if ('template' in options)
            return options.template.f(value);
        else
            return field_utils.format[this.formatType](value, this.field, options);
    },
});

registry
    .add('format', ColumnFormat)
});
/* Examples !!
     <field name="qty_available" widget="format" options="{'template': '{0}%','decimal':2}"/>
     <field name="qty_available" widget="format" options="{'color':'red'}"/>
     <field name="qty_available" widget="format" options="{'css':'color=red'}"/>
     <field name="qty_available" widget="format" options="{'css':'background-color=red;color=blue'}"/>
*/